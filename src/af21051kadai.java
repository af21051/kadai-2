import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class af21051kadai {
    private JButton karaageButton;
    private JButton hanbaGuButton;
    private JButton katudonButton;
    private JButton pizzaButton;
    private JButton udonButton;
    private JButton pastaButton;
    private JButton checkOutButton;
    private JTextPane orderedItemsList;
    private JTextPane sum_price;
    private JPanel root;
    private JTextPane textPane1;
    private JButton button1;
    private JTextPane sum_priceJTextPaneTextPane;

    int sum=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("af21051kadai");
        frame.setContentPane(new af21051kadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order (String food,int price){

        String currentText = orderedItemsList.getText();
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",JOptionPane.YES_NO_OPTION
        );

        if(confirmation==0) {
            JOptionPane.showMessageDialog(null,"order for " + food + " received.");
            orderedItemsList.setText(currentText+food+" "+price+"yen\n");
            sum+=price;
            sum_price.setText(sum+"yen.");

        }

        textPane1.setText("2member:"+sum/2+"yen\n"
                +"3member:"+sum/3+"yen\n"
                +"4member:"+sum/4+"yen\n");


    }
public af21051kadai() {
    karaageButton.setIcon(new javax.swing.ImageIcon(".\\src\\karaage1.jpg"));
    karaageButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Karaage",600);
        }
    });
    hanbaGuButton.setIcon(new javax.swing.ImageIcon(".\\src\\steak.jpg"));
    hanbaGuButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Steak",1200);
        }
    });
    katudonButton.setIcon(new javax.swing.ImageIcon(".\\src\\katudon.jpg"));
    katudonButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Katudon",650);
        }
    });
    pizzaButton.setIcon(new javax.swing.ImageIcon(".\\src\\pizza.jpg"));
    pizzaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Pizza",500);
        }
    });
    udonButton.setIcon(new javax.swing.ImageIcon(".\\src\\udon.jpg"));
    udonButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Udon",450);
        }
    });
    pastaButton.setIcon(new javax.swing.ImageIcon(".\\src\\pasta.jpg"));
    pastaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Pasta",550);
        }
    });
    checkOutButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to Checkout?",
                    "Checkout Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0) {

               JOptionPane.showMessageDialog(null,
                        "Thank you!"+ "The total price is" + sum + "yen");
                sum = 0;

                orderedItemsList.setText(null);
                sum_price.setText("0");
                textPane1.setText("0");




            }

        }
    });

    button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to cancel?",
                    "cancel Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0) {


                sum = 0;

                orderedItemsList.setText(null);
                sum_price.setText("0");
                textPane1.setText("0");

            }}
    });
}
}
